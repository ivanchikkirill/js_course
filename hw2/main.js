const fs = require("fs");

var previous_state = [];

function callback(testFolder = '..') {
    let state = [];
    fs.readdirSync('..')
        .forEach((file) => {
            state.push(file);
        });

    let deleted = previous_state.filter(x => !state.includes(x));
    let allocated = state.filter(x => !previous_state.includes(x));

    for (let file in deleted) {
        console.log(deleted[file] + ' - deleted')
    }
    for (let file in allocated) {
        console.log(allocated[file] + ' - allocated')
    }
    previous_state = [...state];
}

setInterval(callback, 2000);
